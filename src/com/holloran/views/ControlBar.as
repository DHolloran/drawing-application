package com.holloran.views
{
	import com.holloran.interfaces.ISprite;
	import com.holloran.managers.SliderManager;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;

	import libs.ControlBarBase;

	public class ControlBar extends ControlBarBase
	{

		private var _strokePercent:Number;
		private var _shapeWidthPercent:Number;
		private var _shapeHeightPercent:Number;
		private var _shapeRadiusPercent:Number;
		private var _shapeButtons:Array;
		private var _colorButtons:Array;
		private var _circleOn:Boolean;
		private var _squareOn:Boolean;
		private var _rectOn:Boolean;
		private var _strokeOn:Boolean;
		private var _strokeColorOn:Boolean;
		private var _fillColorOn:Boolean;
		private var _strokeSizeSlider:Slider;
		private var _widthSlider:Slider;
		private var _squareSlider:Slider
		private var _radiusSlider:Slider;
		private var _heightSlider:Slider;
		private var _sm1:SliderManager;
		private var _sm2:SliderManager;
		private var _sm3:SliderManager;
		private var _sm4:SliderManager;
		private var _sm5:SliderManager;
		private var _sliders:Array;
		private var _strokeSize:Number;
		private var _shapeWidth:Number;
		private var _sqSize:Number;
		private var _shapeHeight:Number;
		private var _shapeRadius:Number;

		public function ControlBar()
		{
			super ();

			setup ();

		}

		private function setup():void
		{
			_sliders = [];
			_strokeSize = 0;
			_shapeWidth = 0;
			_shapeHeight = 0;
			_shapeRadius = 0;

			circleSliders ();

			this.ActiveColor.btnStrokeActiveColor.mouseChildren = false;
			this.ActiveColor.btnStrokeActiveColor.buttonMode = true;
			this.ActiveColor.tfStrokeColor.stop ();
			_strokeColorOn = false;
			this.ActiveColor.btnStrokeActiveColor.addEventListener ( MouseEvent.CLICK, chooseStrokeColor );

			//Fill Active Color
			this.ActiveColor.btnFillActiveColor.mouseChildren = false;
			this.ActiveColor.btnFillActiveColor.buttonMode = true;
			this.ActiveColor.tfFillColor.gotoAndStop ( 2 );
			_fillColorOn = true;
			this.ActiveColor.btnFillActiveColor.addEventListener ( MouseEvent.CLICK, chooseFillColor );

			_colorButtons = [ this.ActiveColor.tfStrokeColor, this.ActiveColor.tfFillColor ]

			//Circle Button
			this.btnChooseCircle.mouseChildren = false;
			this.btnChooseCircle.buttonMode = true;
			this.btnChooseCircle.gotoAndStop ( 2 );
			_circleOn = true;
			this.btnChooseCircle.addEventListener ( MouseEvent.CLICK, chooseCircleClick );

			//Square Button
			this.btnChooseSquare.mouseChildren = false;
			this.btnChooseSquare.buttonMode = true;
			this.btnChooseSquare.stop ();
			_squareOn = false;
			this.btnChooseSquare.addEventListener ( MouseEvent.CLICK, chooseSquareClick );

			//Rectangle Button
			this.btnChooseRect.mouseChildren = false;
			this.btnChooseRect.buttonMode = true;
			this.btnChooseRect.stop ();
			_rectOn = false;
			this.btnChooseRect.addEventListener ( MouseEvent.CLICK, chooseRectClick );

			//Stroke Button
			this.btnChooseStroke.mouseChildren = false;
			this.btnChooseStroke.buttonMode = true;
			this.btnChooseStroke.stop ();
			_strokeOn = false;
			this.btnChooseStroke.addEventListener ( MouseEvent.CLICK, chooseStrokeClick );

			_shapeButtons = [ btnChooseCircle, btnChooseSquare, btnChooseRect, btnChooseStroke ];

			//Clear Canvas Button
			this.btnClearCanvas.mouseChildren = false;
			this.btnClearCanvas.buttonMode = true;

		}

		private function circleSliders():void
		{
			var x:Number = 255;
			removeSliders ();
			this.SliderText.gotoAndStop ( 1 );
			createRadiusSlider ( x, 30 );
			createStrokeSlider ( x, 50 );

		}

		private function squareSliders():void
		{
			var x:Number = 255;
			removeSliders ();
			this.SliderText.gotoAndStop ( 2 );
			createSquareSlider ( x, 30 );
			createStrokeSlider ( x, 50 );

		}

		private function rectangleSliders():void
		{
			var x:Number = 255;
			removeSliders ();
			this.SliderText.gotoAndStop ( 3 );
			createHeightSlider ( x, 25 );
			createWidthSlider ( x, 40 );
			createStrokeSlider ( x, 55 );

		}

		private function strokeSliders():void
		{
			var x:Number = 255;
			removeSliders ();
			createStrokeSlider ( x, 40 );
			this.SliderText.gotoAndStop ( 4 );

		}

		private function removeSliders():void
		{
			for each ( var s:Slider in _sliders )
			{
				removeChild ( s );
			}
			_sliders = [];

		}

		private function createWidthSlider( x:int, y:int ):void
		{
			//Width Slider
			_widthSlider = new Slider ();
			_widthSlider.x = x;
			_widthSlider.y = y;
			this.SliderText.tfWidthPercent.text = "0%";
			addChild ( _widthSlider );
			_sliders.push ( _widthSlider );
			_sm1 = new SliderManager ();
			_sm1.setUpAssets ( _widthSlider.mc_track, _widthSlider.mc_handle );
			_sm1.addEventListener ( Event.CHANGE, changeWidth );

		}

		private function createSquareSlider( x:int, y:int ):void
		{
			//Square Slider
			_squareSlider = new Slider ();
			_squareSlider.x = x;
			_squareSlider.y = y;
			this.SliderText.tfSquarePercent.text = "0%";
			addChild ( _squareSlider );
			_sliders.push ( _squareSlider );
			_sm5 = new SliderManager ();
			_sm5.setUpAssets ( _squareSlider.mc_track, _squareSlider.mc_handle );
			_sm5.addEventListener ( Event.CHANGE, changeSquare );

		}

		private function createHeightSlider( x:int, y:int ):void
		{
			//Height Slider
			_heightSlider = new Slider ();
			_heightSlider.x = x;
			_heightSlider.y = y;
			this.SliderText.tfHeightPercent.text = "0%";
			addChild ( _heightSlider );
			_sliders.push ( _heightSlider );
			_sm2 = new SliderManager ();
			_sm2.setUpAssets ( _heightSlider.mc_track, _heightSlider.mc_handle );
			_sm2.addEventListener ( Event.CHANGE, changeHeight );

		}

		private function createRadiusSlider( x:int, y:int ):void
		{
			//Radius Slider
			_radiusSlider = new Slider ();
			_radiusSlider.x = x;
			_radiusSlider.y = y;
			this.SliderText.tfRadiusPercent.text = "0%";
			addChild ( _radiusSlider );
			_sliders.push ( _radiusSlider );
			_sm2 = new SliderManager ();
			_sm2.setUpAssets ( _radiusSlider.mc_track, _radiusSlider.mc_handle );
			_sm2.addEventListener ( Event.CHANGE, changeRadius );

		}

		private function createStrokeSlider( x:int, y:int ):void
		{
			//Stroke Slider
			_strokeSizeSlider = new Slider ();
			_strokeSizeSlider.x = x;
			_strokeSizeSlider.y = y;
			this.SliderText.tfStrokePercent.text = "0%";
			addChild ( _strokeSizeSlider );
			_sliders.push ( _strokeSizeSlider );
			_sm2 = new SliderManager ();
			_sm2.setUpAssets ( _strokeSizeSlider.mc_track, _strokeSizeSlider.mc_handle );
			_sm2.addEventListener ( Event.CHANGE, changeStroke );

		}

		//-----------------Slider Events--------------------------------------
		private function changeStroke( e:Event ):void
		{
			var sm:SliderManager = SliderManager ( e.currentTarget );
			var p:Number = Math.ceil ( sm.percent * 100 );
			_strokeSize = ( sm.percent * 10 );
			this.SliderText.tfStrokePercent.text = p + "%";

		}

		private function changeHeight( e:Event ):void
		{
			var sm:SliderManager = SliderManager ( e.currentTarget );
			var p:Number = Math.ceil ( sm.percent * 100 );
			_shapeHeight = ( sm.percent * 100 );
			this.SliderText.tfHeightPercent.text = p + "%";

		}

		private function changeRadius( e:Event ):void
		{
			var sm:SliderManager = SliderManager ( e.currentTarget );
			var p:Number = Math.ceil ( sm.percent * 100 );
			_shapeRadius = ( sm.percent * 50 );
			this.SliderText.tfRadiusPercent.text = p + "%";

		}

		private function changeWidth( e:Event ):void
		{
			var sm:SliderManager = SliderManager ( e.currentTarget );
			var p:Number = Math.ceil ( sm.percent * 100 );
			_shapeWidth = ( sm.percent * 100 );
			this.SliderText.tfWidthPercent.text = p + "%";

		}

		private function changeSquare( e:Event ):void
		{
			var sm:SliderManager = SliderManager ( e.currentTarget );
			var p:Number = Math.ceil ( sm.percent * 100 );
			_sqSize = ( sm.percent * 100 );
			this.SliderText.tfSquarePercent.text = p + "%";

		}

//---------------- Choose Colors--------------//		
		private function chooseFillColor( event:MouseEvent ):void
		{
			//Sets the Fill color state

			if ( _fillColorOn == false )
			{
				clearColorButtons ();
				this.ActiveColor.tfFillColor.gotoAndStop ( 2 );
				_fillColorOn = true;
			}
			else
			{
				clearColorButtons ();
				this.ActiveColor.tfFillColor.gotoAndStop ( 1 );
				_fillColorOn = false;
			}

		}

		private function chooseStrokeColor( event:MouseEvent ):void
		{
			//Sets the Stroke color state

			if ( _strokeColorOn == false )
			{
				clearColorButtons ();
				this.ActiveColor.tfStrokeColor.gotoAndStop ( 2 );
				_strokeColorOn = true;
			}
			else
			{
				clearColorButtons ();
				this.ActiveColor.tfStrokeColor.gotoAndStop ( 1 );
				_fillColorOn = false;
			}

		}

//----------- Clears Buttons -------------------------------
		private function clearColorButtons():void
		{
			//Clears the Color buttons

			for each ( var btn:MovieClip in _colorButtons )
			{
				if ( btn.currentFrame == 2 )
				{
					this.ActiveColor.tfFillColor.gotoAndStop ( 1 );
					this.ActiveColor.tfStrokeColor.gotoAndStop ( 1 );
					_fillColorOn = false;
					_strokeColorOn = false;
				}
			}

		}

		private function clearShapeButtons():void
		{
			//Clears the Shape buttons

			for each ( var btn:MovieClip in _shapeButtons )
			{
				if ( btn.currentFrame == 2 )
				{
					btn.gotoAndStop ( 1 );
					_circleOn = false;
					_squareOn = false;
					_rectOn = false;
					_strokeOn = false;
				}
			}

		}

//---------------------- Chooses Buttons ----------------------------		
		private function chooseCircleClick( e:MouseEvent ):void
		{
			//Sets the Circle button state

			if ( _circleOn == false )
			{
				clearShapeButtons ();
				circleSliders ();
				_circleOn = true;
				this.btnChooseCircle.gotoAndStop ( 2 );
				this.ActiveColor.gotoAndStop ( 1 );
			}
			else
			{
				clearShapeButtons ();
				_circleOn = false;
			}

		}

		private function chooseSquareClick( e:MouseEvent ):void
		{
			//Sets the Square button state

			if ( _squareOn == false )
			{
				clearShapeButtons ();
				squareSliders ();
				_squareOn = true;
				this.btnChooseSquare.gotoAndStop ( 2 );
			}
			else
			{
				clearShapeButtons ();
				_squareOn = false;
			}

		}

		private function chooseStrokeClick( e:MouseEvent ):void
		{
			//Sets the Stroke button state

			if ( _strokeOn == false )
			{
				clearShapeButtons ();
				strokeSliders ();
				_strokeOn = true;
				this.btnChooseStroke.gotoAndStop ( 2 );
			}
			else
			{
				clearShapeButtons ();
				_strokeOn = false;
			}

		}

		private function chooseRectClick( e:MouseEvent ):void
		{
			//Sets the Rectangle button state
			if ( _rectOn == false )
			{
				clearShapeButtons ();
				rectangleSliders ();
				_rectOn = true;
				this.btnChooseRect.gotoAndStop ( 2 );
			}
			else
			{
				clearShapeButtons ();
				_rectOn = false;
			}

		}

//------------------ Getters/Setters -------------------------
		public function get strokeSize():Number
		{
			return _strokeSize;

		}

		public function get shapeWidth():Number
		{
			return _shapeWidth;

		}

		public function get shapeHeight():Number
		{
			return _shapeHeight;

		}

		public function get shapeRadius():Number
		{
			return _shapeRadius;

		}

		public function get circleOn():Boolean
		{
			return _circleOn;

		}

		public function get squareOn():Boolean
		{
			return _squareOn;

		}

		public function get rectOn():Boolean
		{
			return _rectOn;

		}

		public function get strokeOn():Boolean
		{
			return _strokeOn;

		}

		public function get strokeColorOn():Boolean
		{
			return _strokeColorOn;

		}

		public function get fillColorOn():Boolean
		{
			return _fillColorOn;

		}

		public function get sqSize():Number
		{
			return _sqSize;

		}


	}
}