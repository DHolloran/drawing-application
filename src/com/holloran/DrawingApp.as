package com.holloran
{
	import com.holloran.events.ColorPickerEvent;
	import com.holloran.views.ControlBar;
	import com.holloran.views.Slider;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.ui.Mouse;

	import libs.CustomCursor;
	import libs.DrawingAppBase;

	public class DrawingApp extends DrawingAppBase
	{
		private var _cp:ColorPicker;
		private var _cb:ControlBar;
		private var _strokeSize:Number;
		private var _shapeWidth:Number;
		private var _sqSize:Number;
		private var _shapeHeight:Number;
		private var _shapeRadius:Number;
		private var _canvas:Sprite;
		private var _drawingShape:DrawingShapes;
		private var _shapes:Array;
		private var _strokeColor:uint;
		private var _fillColor:uint;
		private var _stroke:int;
		private var _shape:int;
		private var _draw:Boolean;

		public function DrawingApp()
		{
			super ();
			init ();

		}

		private function init():void
		{

			_shapes = [];

			_strokeColor = 0x000000;
			_fillColor = 0x0000FF;

			_draw = false;
			//Canvas
			_canvas = new Sprite ();
			this.addChild ( _canvas );
			_canvas.mask = this.MyMask;

			this.addEventListener ( MouseEvent.MOUSE_MOVE, draw );
			this.addEventListener ( MouseEvent.CLICK, drawOnCanvas );
			this.addEventListener ( MouseEvent.MOUSE_DOWN, startDraw );
			this.addEventListener ( MouseEvent.MOUSE_UP, stopDraw );

			//Control Bar
			_cb = new ControlBar ();
			_cb.x = 5;
			_cb.y = 280;
			this.addChild ( _cb );
			_cb.ActiveColor.stop ();
			_cb.btnClearCanvas.addEventListener ( MouseEvent.CLICK, clearCanvas );
			setupShapes ();

			//Color Picker
			_cp = new ColorPicker ( "assets/images/colorPicker.png" );
			_cp.x = 483;
			_cp.y = 5;
			_cb.addChild ( _cp );
			_cp.addEventListener ( ColorPickerEvent.COLOR_CHANGE, colorChange );


		}

		private function startDraw( e:MouseEvent ):void
		{
			_draw = true;
			_canvas.graphics.moveTo ( this.mouseX, this.mouseY );

		}

		private function stopDraw( e:MouseEvent ):void
		{
			_draw = false;

		}

		private function setupShapes():void
		{
			_shapeWidth = _cb.shapeWidth;
			_sqSize = _cb.sqSize;
			_shapeHeight = _cb.shapeHeight;
			_shapeRadius = _cb.shapeRadius;
			_strokeSize = _cb.strokeSize;

		}



		private function clearCanvas( event:MouseEvent ):void
		{
			for each ( var s:DrawingShapes in _shapes )
			{
				_canvas.removeChild ( s );
				_canvas.graphics.clear ();
				_shapes = [];
			}

		}

		private function drawOnCanvas( e:MouseEvent ):void
		{

			if ( _cb.circleOn )
			{
				setupShapes ();
				_drawingShape = new DrawingShapes ();
				_drawingShape.drawCircle ( _fillColor, _strokeSize, _strokeColor, _shapeRadius );
				_drawingShape.x = e.stageX;
				_drawingShape.y = e.stageY;
				_canvas.addChild ( _drawingShape );
				_shapes.push ( _drawingShape );
			}
			else if ( _cb.squareOn )
			{
				setupShapes ();
				_drawingShape = new DrawingShapes ();
				_drawingShape.drawSquare ( _fillColor, _strokeSize, _strokeColor, _sqSize );
				_drawingShape.x = e.stageX;
				_drawingShape.y = e.stageY;
				_canvas.addChild ( _drawingShape );
				_shapes.push ( _drawingShape );
			}
			else if ( _cb.rectOn )
			{
				setupShapes ();
				_drawingShape = new DrawingShapes ();
				_drawingShape.drawRect ( _fillColor, _strokeSize, _strokeColor, _shapeWidth, _shapeHeight );
				_drawingShape.x = e.stageX;
				_drawingShape.y = e.stageY;
				_canvas.addChild ( _drawingShape );
				_shapes.push ( _drawingShape );
			}

		}

		private function draw( event:MouseEvent ):void
		{
			if ( _cb.strokeOn && _draw )
			{
				setupShapes ();

				_canvas.graphics.lineStyle ( _strokeSize, _strokeColor );
				_canvas.graphics.lineTo ( this.mouseX, this.mouseY );

			}

		}

//-------------- Color Picker Event -------------		
		private function colorChange( e:ColorPickerEvent ):void
		{
			var ct:ColorTransform = new ColorTransform ();
			ct.color = e.color;

			if ( _cb.strokeColorOn == true )
			{
				_cb.ActiveColor.btnStrokeActiveColor.transform.colorTransform = ct;
				_strokeColor = e.color;
				trace ( "Stroke color:" + e.color );
			}
			else
			{
				_cb.ActiveColor.btnFillActiveColor.transform.colorTransform = ct;
				_fillColor = e.color;
				trace ( "Fill color:" + e.color )
			}


		}



	}
}