package
{

	import com.holloran.DrawingApp;

	import flash.display.Sprite;

	[SWF( width="600", height="400", backgroundColor="0xFFFFFF", frameRate="24" )]
	public class Main extends Sprite
	{


		public function Main()
		{
			super ();

			var drawingApp:DrawingApp = new DrawingApp ();
			drawingApp.x = ( stage.stageWidth / 2 ) - ( drawingApp.width / 2 );
			drawingApp.y = ( stage.stageHeight / 2 ) - ( drawingApp.height / 2 );
			this.addChild ( drawingApp );

		}
	}

}
